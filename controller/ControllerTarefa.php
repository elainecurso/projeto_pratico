<?php

include_once '../configuracao/Import.php';
Import::controller('AbstractController.php');
Import::dao('TarefaDao.php');

class ControllerTarefa extends AbstractController{
    
    // Método responsavel por executar a lógica de cadastrar uma tarefa
    public function cadastrarTarefa(){
        $tarefaDao = new TarefaDao();
        $tarefa = new Tarefa();
        if(isset($_GET['cadastrar'])){
            $tarefa->setTarefa($_GET['tarefa']);
            $tarefa->setDescricao($_GET['descricao']);
            $tarefa->setPrazo($_GET['prazo']);
            $tarefa->setPrioridade($_GET['prioridade']);

            $tarefaDao->cadastrarTarefa($tarefa,parent::getSession()->get('idUsuario'));
        }
    }
    
    // Método responsavel por executar a lógica de atualizar uma tarefa
    public function updateTarefa(){
        $tarefaDao = new TarefaDao();
        $tarefa = new Tarefa();
        if(isset($_GET['editarTarefa'])){
        $tarefa->setTarefa($_GET['tarefa']);
        $tarefa->setDescricao($_GET['descricao']);
        $tarefa->setPrazo($_GET['prazo']);
        $tarefa->setPrioridade($_GET['prioridade']);
    
        $tarefaDao->updateTarefa((int)parent::getSession()->get('idTarefa'),$tarefa);
        $tarefaDao->updateDescricao((int)parent::getSession()->get('idTarefa'), $tarefa);
        $tarefaDao->updatePrazo(parent::getSession()->get('idTarefa'), $tarefa);
        $tarefaDao->updatePrioridade((int) parent::getSession()->get('idTarefa'), $tarefa);
        header('Location: ../view/tarefa.php');
        }
    }
    
    // Método responsavel por imprimir as tarefas cadastradas no tela do usuário
    public function mostrarTarefas(){
        $tarefa = new TarefaDao();
        $tarefaIds = $tarefa->buscarTarefaPorIdUsuario(parent::getSession()->get('idUsuario'));
        foreach($tarefaIds as $tarefaId){
            echo "<tr>
            <td>".$tarefaId['tarefa']."</td>
            <td>".$tarefaId['descricao']."</td>
            <td>".$tarefaId['prazo']."</td>
            <td>".self::editarPrioridade($tarefaId['prioridade'])."</td>
            <td><button type='submit' value=".$tarefaId['id']." name='idExcluir' class='btn btn-danger'>Excluir</button></td>
            <td><button type='submit' value=".$tarefaId['id']." name='idEditar' class='btn btn-warning'>Editar</button></td>
            </tr>";
        }
    }
    
    // Irá tratar as informações passadas pelo formulário e transformar no nível de prioridade desejada
    public function editarPrioridade($valor){
        switch ($valor){
        case '1':
            return 'baixo';
            break;
        case '2':
            return  'medio';
            break;
        case '3':
            return 'alto';
            break;
        default:
            return false;
        }
    }
    
    // Método que irá excluir uma tarefa
    public function deletarTarefa(){
        $tarefa = new TarefaDao();
        if(isset($_GET['idExcluir'])){
            $tarefa->deletarTarefa((int) $_GET['idExcluir']);
        }
   }
    
    
   // Método que irá redirecionar para a página de edição
    public function redirecionarPaginaEditar(){
        if(isset($_GET['idEditar'])){
            parent::getSession()->set('idTarefa',(int) $_GET['idEditar']);
            header('Location: ../view/editar.php');
        }
    }
    
    // Método responsavel pela saida da página
    public function sair(){
        if(isset($_GET['sair'])){
            parent::getSession()->destroy();
            header('Location: ../view/login.php');
        }
    }
    
}



?>
