<?php 
include_once '../configuracao/Import.php';
Import::dao('LoginDao.php');
Import::controller('AbstractController.php');

// Esta classe será a ligação da view com a model(dao, bean).
class ControllerLogin extends AbstractController{
    
    // Método para verificar se os compos do login estão todos preenchidos
    public function verificarLogin($var1,$var2){
        if($var1 == NULL || $var2 == NULL){
            return false;
        }
        else{
            return true;
        }
    }
   
    // Método para verificar se os compos de cadastro estão todos preenchidos
    public function verificarCadastro($var1,$var2,$var3,$var4){
        if($var1 == NULL || $var2 == NULL || $var3 == NULL || $var4 == NULL){
            return false;
        }
        else{
            return true;
        }
    }
    
    // Executa toda lógica de verificação dos dados do login e redirecionamento da página
    public function executarLogin(){
        $loginDao = new LoginDao();
        $usuario = new Usuario();
        if(isset($_GET['submit'])){
            if(self::verificarLogin($_GET['login'], $_GET['senha'])){
               
                $usuario->setLogin($_GET['login']);
                $usuario->setSenha($_GET['senha']);
        
                $idUsuario = $loginDao->verificarPerfil($usuario);
            
                if($idUsuario){
                    parent::getSession()->set('idUsuario', (int) $idUsuario); // Neste momento é utilizado o conceito de sessão, onde o Id do usuario será importante para usar em lógicas futuras
                    header('Location: ../view/tarefa.php'); // Redirecionamento da página
                }
            }
        }
     }
        
     // Executa toda a lógica de persistência dos dados para gravar o novo usuario.
        public function cadastrarUsuario(){
            $loginDao = new LoginDao();
            $usuario = new Usuario();
            $pessoa = new Pessoa();
             
            if(isset($_GET['cadastro'])){
                if(self::verificarCadastro( $_GET['nome'], $_GET['email'], $_GET['login'], $_GET['senha'])){
                
                    $pessoa->setNome($_GET['nome']);
                    $pessoa->setEmail($_GET['email']);
                    $usuario->setLogin($_GET['login']);
                    $usuario->setSenha($_GET['senha']);
                
                    $cadastro = $loginDao->cadastrar($usuario,$pessoa);
                    if($cadastro == 'true'){
                        header('Location: ../view/login.php');
                    }
                }
            }
        }
        
        

}



?>